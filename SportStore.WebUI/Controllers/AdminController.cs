﻿using System.Linq;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;

namespace SportStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IProductRepository repository;

        public AdminController(IProductRepository repo) {
            repository = repo;
        }

        public ViewResult Index() {
            return View(repository.Products);
        }

        public ViewResult Edit(int productId) {
            Product product = repository.Products.FirstOrDefault(p => p.ProductID == productId);

            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduct(product);
                TempData["message"] = string.Format("{0} has been saved", product.Name);
                return RedirectToAction("Index");
            }
            else {
                return View(product);
            }
       }

       public ViewResult Create() {
           return View("Edit", new Product());     
       }

       public ActionResult Duplicate(int productId)
       {
           Product product = repository.Products.FirstOrDefault(p => p.ProductID == productId);
           Product newProduct = new Product { 
                Name = product.Name + "--copy",
                Description = product.Description,
                Price = product.Price,
                Category = product.Category
           };
           repository.SaveProduct(newProduct);
           return RedirectToAction("Index");
       }

       [HttpPost]
       public ActionResult Delete(int productId) {
           Product deletedProduct = repository.DeleteProduct(productId);
           if (deletedProduct != null) {
               TempData["message"] = string.Format("{0} was deleted", deletedProduct.Name);
           }
           return RedirectToAction("Index");
       } 
    }
}